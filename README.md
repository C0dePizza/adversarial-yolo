# Adversarial YOLO
forked from https://gitlab.com/EAVISE/adversarial-yolo

# Setup (Linux)
no gpu is necessary, modifications have been made to use the cpu version of torch.

no training is required, so it works out.

weights file and input images are included in this repo.

```
python -m venv venv
source venv/bin/activate
pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu
pip install tensorboardX tensorboard
```

# Making predictions on a directory of images
`detect.py` has been modified to enable detection of multiple images outputting a csv file. To run:

```
python detect.py cfg/yolov2.cfg weights/yolo.weights input_images
```

`detect.py` will write `<filename>,<label>,<confidence>` to `predictions.csv` as well as write labeled prediction images to `./predictions/`
